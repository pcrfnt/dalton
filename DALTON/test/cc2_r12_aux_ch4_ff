#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > cc2_r12_aux_ch4_ff.info <<'%EOF%'
   cc2_r12_aux_ch4_ff
   -------------
   Molecule:         Methane (CH4)
   Wave Function:    CC2-R12 / 6-311G**
   Test Purpose:     Check CC2-R12 finite field implementation
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > cc2_r12_aux_ch4_ff.mol <<'%EOF%'
BASIS
6-311G** 6-311++G(3df,3pd)
CH4: CC2-R12/A finite field calculation without symmetry

    4    0
 1      6.    1
C      0.000000000000000   0.000000000000000   0.000000000000000       *
 1      1.    4
H      1.276731000000000   1.276731000000000   1.276731000000000       *
H     -1.276731000000000  -1.276731000000000   1.276731000000000       *
H     -1.276731000000000   1.276731000000000  -1.276731000000000       *
H      1.276731000000000  -1.276731000000000  -1.276731000000000       *
 2      6.    1
C      0.000000000000000   0.000000000000000   0.000000000000000       *
 2      1.    4
H      1.276731000000000   1.276731000000000   1.276731000000000       *
H     -1.276731000000000  -1.276731000000000   1.276731000000000       *
H     -1.276731000000000   1.276731000000000  -1.276731000000000       *
H      1.276731000000000  -1.276731000000000  -1.276731000000000       *
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > cc2_r12_aux_ch4_ff.dal <<'%EOF%'
**DALTON INPUT
.RUN WAVE FUNCTION
.DIRECT
*MOLBAS
.R12AUX
**INTEGRALS
.R12
.DIPLEN
**WAVE FUNCTIONS
.CC
*SCF INPUT
.THRESHOLD
 1.0D-10
*ORBITALS
.MOSTART
 H1DIAG
*CC INPUT
.CC2
.PRINT
 3
.FREEZE
 1 0
.THRENR
 1.0D-12
.THRLEQ
 1.0D-10
.FIELD
 1.0D-02
 XDIPLEN
*CCFOP
.NONREL
.DIPMOM
*CCLR
.OPERAT
XDIPLEN XDIPLEN
*CCQR
.OPERAT
XDIPLEN XDIPLEN XDIPLEN
*R12 INPUT
.CC2
 1A
.BASSCL
 0.5 2.0
**END OF DALTON INPUT
%EOF%

#######################################################################
 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >cc2_r12_aux_ch4_ff.check
cat >>cc2_r12_aux_ch4_ff.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

if $GREP -q "not implemented for parallel calculations" $log; then
   echo "TEST ENDED AS EXPECTED"
   exit 0
fi

#
#            Total SCF   energy:                   -40.1917720926
#            Total MP2-R12/A energy:               -40.3918078442
#            Total CC2-R12/A energy:               -40.3936882553
#

CRIT1=`$GREP "Total MP2-R12/A energy: *-40\.39180784" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=2
ERROR[1]="MP2-R12/A ENERGY NOT CORRECT"
CRIT2=`$GREP "Total CC2-R12/A energy: *-40\.39368825" $log | wc -l`
TEST[2]=`expr   $CRIT2`
CTRL[2]=2
ERROR[2]="CC2-R12/A ENERGY NOT CORRECT"
CRIT3=`$GREP "x *( |0)\.15519992 *( |0)\.3944788" $log | wc -l`
TEST[3]=`expr   $CRIT3`
CTRL[3]=1
ERROR[3]="DIPOLE MOMENT NOT CORRECT"
CRIT4=`$GREP "XDIPLEN .* 15\.531127" $log | wc -l`
TEST[4]=`expr   $CRIT4`
CTRL[4]=1
ERROR[4]="POLARIZABILITY NOT CORRECT"
CRIT5=`$GREP "XDIPLEN .* 3\.3458198" $log | wc -l`
TEST[5]=`expr   $CRIT5`
CTRL[5]=1
ERROR[5]="FIRST HYPER-POLARIZABILITY NOT CORRECT"
#

PASSED=1
for i in 1 2 3 4 5  
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo "${ERROR[i]} ( test = ${TEST[i]}; control = ${CTRL[i]} ); "
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

%EOF%
#######################################################################
